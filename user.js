   function createNewUser() {
    const newUser = {};
  
    let firstName = prompt("Enter your name:");
    let lastName = prompt("Enter your last name:");
    let birthDate = prompt("Enter your date of birth:");
  
    newUser.getFirstName = function() {
      return firstName;
    };
  
    newUser.getLastName = function() {
      return lastName;
    };
  
    newUser.getLogin = function() {
      return (firstName.charAt(0) + lastName).toLowerCase();
    };
  
    newUser.getAge = function() {
      const today = new Date();
      const birthDateArr = birthDate.split(".");
      const birthYear = birthDateArr[2];
      const birthMonth = birthDateArr[1] - 1;
      const birthDay = birthDateArr[0];
      const birthDateObj = new Date(birthYear, birthMonth, birthDay);
      let age = today.getFullYear() - birthDateObj.getFullYear();
      const monthDiff = today.getMonth() - birthDateObj.getMonth();
      if (monthDiff < 0 || (monthDiff === 0 && today.getDate() < birthDateObj.getDate())) {
        age--;
      }
      return age;
    };
  
    newUser.getPassword = function() {
      const passwordYear = birthDate.split(".")[2];
      const password = firstName.charAt(0).toUpperCase() + lastName.toLowerCase() + passwordYear;
      return password;
    };
  
    return newUser;
  }
  const user = createNewUser();
  console.log(user.getFirstName()); 
  console.log(user.getLastName()); 
  console.log(user.getLogin()); 
  console.log(user.getAge()); 
  console.log(user.getPassword()); 
  
  